/*
 * AVISO:
 * Não é aconselhável armazenar senhas não criptografadas em banco de dados.
 * O ideal é armazenar alguma forma de "hash" (MD5, SHA1) dessas senhas.
 * Neste exemplo, não usaremos criptografia, para manter a simplicidade.  
 */
INSERT INTO usuario (username, password) VALUES ('doug.funnie', 'abc');
INSERT INTO usuario (username, password) VALUES ('roger.klotz', 'abc');
INSERT INTO usuario (username, password) VALUES ('root', 'toor');

INSERT INTO cliente (nome, email, usuario) VALUES ('Douglas Funnie', 'doug@jumbo.com', 1);
INSERT INTO cliente (nome, email, usuario) VALUES ('Roger Klotz', 'roger@jumbo.com', 2);


INSERT INTO produto (descricao, preco) VALUES ('Esmeril De Bancada 300w 6" Schulz', 225);
INSERT INTO produto (descricao, preco) VALUES ('Máquina de solda inverter Titanium TI150 220V', 345);
INSERT INTO produto (descricao, preco) VALUES ('Micro Retifica Lixadeira Maleta 62 Peças', 130);
INSERT INTO produto (descricao, preco) VALUES ('Mascara De Solda Automatica Auto Escurecimento Titanium', 74);