package edu.ifsp.lojinha.controlador.comprar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Produto;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class EsvaziarCarrinho implements Command {

	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Produto> carrinho = new ArrayList<Produto>();
		HttpSession session = request.getSession();
		session.setAttribute("carrinho", carrinho);			
		
		response.sendRedirect("comprar.sec");
	}

}
