package edu.ifsp.lojinha.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.modelo.Usuario;

public class ProdutoDAO {
	private static String MISSING_GENERATED_KEY = "ID gerado não foi recuperado. [tabela: %s]";
	
	List<Produto> produtos = List.of(
			new Produto(1, "Batata"), 
			new Produto(2, "Cebola"), 
			new Produto(3, "Alho")
			);

	public List<Produto> listarTodos() {
		return produtos;
	}

	public Produto save(Produto produto) throws PersistenceException {
		try (Connection conn = DatabaseConnector.getConnection()) {
			conn.setAutoCommit(false);
			try {
				PreparedStatement ps = conn.prepareStatement(
						"INSERT INTO produto (descricao, preco) VALUES (?, ?);", 
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, produto.getDescricao());
				ps.setBigDecimal(2, produto.getPreco());
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					produto.setId(rs.getInt(1));
				} else {
					conn.rollback();
					throw new PersistenceException(String.format(MISSING_GENERATED_KEY, "product"));
				}
				ps.close();
				rs.close();
				conn.commit();
				
			} catch (SQLException e) {
				conn.rollback();
				throw e;
			}
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return produto;
		
	}
	
	public boolean exists(String descricao) throws PersistenceException {
		boolean found = false;
		
		try (Connection conn = DatabaseConnector.getConnection()) {			
			PreparedStatement ps = conn.prepareStatement(
					"SELECT count(*) FROM produto WHERE descricao = ?;");
			ps.setString(1, descricao);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next() && rs.getInt(1) > 0) {				
				found = true;
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return found;
	}
	
	public Produto findById(int idProduto) throws PersistenceException {
		Produto produto = null;
		try (Connection conn = DatabaseConnector.getConnection()) {
			PreparedStatement ps = conn.prepareStatement(
					"SELECT p.id , p.descricao, p.preco "
					+ "FROM produto p WHERE p.id = ?");
			
			ps.setInt(1, idProduto);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				produto = mapRow(rs);				
			}
			rs.close();
			ps.close();			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return produto;
	}
	
	private Produto mapRow(ResultSet rs) throws SQLException {
		Produto produto = new Produto(rs.getString("descricao"), rs.getBigDecimal("preco"));
		produto.setId(rs.getInt("id"));
		return produto;
	}
	
	public Produto remove(int idProduto) throws PersistenceException {
		
		try (Connection conn = DatabaseConnector.getConnection()) {
			conn.setAutoCommit(false);
			try {
				
				Produto produto = findById(idProduto);
				
				PreparedStatement ps = conn.prepareStatement(
						"Delete From produto p Where p.id = ?;", 
						Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, produto.getId());
				ps.executeUpdate();
				ps.close();
				
				conn.commit();
				
				return produto;
				
			} catch (SQLException e) {
				conn.rollback();
				throw e;
			}
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		
	}

	public List<Produto> listAll() throws PersistenceException {
		List<Produto> lista = new LinkedList<>();
		try (Connection conn = DatabaseConnector.getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT p.id, p.descricao, p.preco "
					+ "FROM produto p");
			
			while (rs.next()) {
				Produto produto = mapRow(rs);				
				lista.add(produto);
			}
			
			rs.close();
			stmt.close();			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return lista;
	}

}
