<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Cadastro de Produtos</title>
<link rel="stylesheet" href="css/cadastro.css">
<script type="text/javascript" src="js/cadastro.js"></script>
</head>
<body>

<h1>Cadastro de Produtos</h1>

<c:if test="${requestScope.saved}">
<p><span class="success flash">Dados salvos!</span></p>
</c:if>

<c:if test="${requestScope.notfound}">
<p><span class="warning">Produto não encontrado!</span></p>
</c:if>

<form action="produto.sec?cmd=adicionar" method="post">
	
	<p>
		<label for="descricao">Descrição: </label>
		<input type="text" id="descricao" name="descricao" width="40" required maxlength="100" value="${requestScope.produto.descricao}">
	</p>
	
	<p>
		<label for="nome">Preço: </label>
		<input type="text" id="preco" name="preco" width="40" required maxlength="40" value="${requestScope.produto.preco}">
	</p>

	<br>
	<button type="submit">Salvar</button>
</form>

</body>
</html>