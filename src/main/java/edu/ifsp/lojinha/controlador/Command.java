package edu.ifsp.lojinha.controlador;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface Command {
	void processar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
