package edu.ifsp.lojinha.controlador.cliente;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.modelo.Usuario;
import edu.ifsp.lojinha.persistencia.ClienteDAO;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.UsuarioDAO;
import edu.ifsp.lojinha.web.FlashUtil;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class SalvarCliente implements Command {

	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* mapeando parâmetros do request para objetos do modelo de dados */
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");

		Cliente cliente = new Cliente(nome, email);
		Usuario usuario = new Usuario();
		usuario.setUsername(username);
		usuario.setPassword(password);
		cliente.setUsuario(usuario);
		
		/* salvando objetos no banco de dados */
		try {
			UsuarioDAO usuarioDao = new UsuarioDAO();		
			if (!usuarioDao.exists(username)) {
				ClienteDAO dao = new ClienteDAO();
				dao.save(cliente);
				
				FlashUtil.setAttribute(request, "cliente:salvar", "saved", true);
				response.sendRedirect("cadastrar?cmd=detalhar&id=" + cliente.getId());
				
			} else {

				request.setAttribute("userExists", true);
				RequestDispatcher rd = request.getRequestDispatcher("cadastro.jsp");
				rd.forward(request, response);
			}
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}		
	}

}
