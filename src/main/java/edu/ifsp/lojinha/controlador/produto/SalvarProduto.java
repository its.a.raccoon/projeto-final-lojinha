package edu.ifsp.lojinha.controlador.produto;

import java.io.IOException;
import java.math.BigDecimal;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import edu.ifsp.lojinha.web.FlashUtil;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class SalvarProduto implements Command {

	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String descricao = request.getParameter("descricao");
		String preco = request.getParameter("preco");
		preco.replaceAll(",","");
		BigDecimal bd = new BigDecimal(preco);
		Produto produto = new Produto(descricao, bd);
		
		try {
			ProdutoDAO produtoDao = new ProdutoDAO();		
			if (!produtoDao.exists(descricao)) {
				ProdutoDAO dao = new ProdutoDAO();
				dao.save(produto);
				
				
			}
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}		
	}

}
