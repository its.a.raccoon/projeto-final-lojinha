package edu.ifsp.lojinha.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.modelo.Usuario;

public class ClienteDAO {
	private static String MISSING_GENERATED_KEY = "ID gerado não foi recuperado. [tabela: %s]";
	
	public Cliente save(Cliente cliente) throws PersistenceException {
		try (Connection conn = DatabaseConnector.getConnection()) {
			/* iniciando transação */
			conn.setAutoCommit(false);
			try {
				PreparedStatement ps = conn.prepareStatement(
						"INSERT INTO usuario (username, password) VALUES (?, ?);", 
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, cliente.getUsuario().getUsername());
				ps.setString(2, cliente.getUsuario().getPassword());
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					cliente.getUsuario().setId(rs.getLong(1));
				} else {
					conn.rollback();
					throw new PersistenceException(String.format(MISSING_GENERATED_KEY, "user"));
				}
				ps.close();
				rs.close();
				
				ps = conn.prepareStatement(
						"INSERT INTO cliente (nome, email, usuario) VALUES (?, ?, ?);",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, cliente.getNome());
				ps.setString(2, cliente.getEmail());
				ps.setLong(3, cliente.getUsuario().getId());
				ps.executeUpdate();
				rs = ps.getGeneratedKeys();
				if (rs.next()) {
					cliente.setId(rs.getLong(1));
				} else {
					conn.rollback();
					throw new PersistenceException(String.format(MISSING_GENERATED_KEY, "cliente"));
				}
				ps.close();
				
				conn.commit();
				
			} catch (SQLException e) {
				conn.rollback();
				throw e;
			}
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return cliente;
	}
	
	public List<Cliente> listAll() throws PersistenceException {
		List<Cliente> lista = new LinkedList<>();
		try (Connection conn = DatabaseConnector.getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT c.id cid, c.nome, c.email, u.username, u.id uid "
					+ "FROM cliente c, usuario u WHERE c.usuario = u.id "
					+ "ORDER BY c.nome;");
			
			while (rs.next()) {
				Cliente cliente = mapRow(rs);				
				lista.add(cliente);
			}
			
			rs.close();
			stmt.close();			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return lista;
	}

	private Cliente mapRow(ResultSet rs) throws SQLException {
		Cliente cliente = new Cliente(rs.getString("nome"), rs.getString("email"));
		cliente.setId(rs.getLong("cid"));
		Usuario usuario = new Usuario();
		usuario.setId(rs.getLong("uid"));
		usuario.setUsername(rs.getString("username"));
		cliente.setUsuario(usuario);
		return cliente;
	}

	public Cliente findById(int idCliente) throws PersistenceException {
		Cliente cliente = null;
		try (Connection conn = DatabaseConnector.getConnection()) {
			PreparedStatement ps = conn.prepareStatement(
					"SELECT c.id cid, c.nome, c.email, u.username, u.id uid "
					+ "FROM cliente c, usuario u WHERE c.usuario = u.id "
					+ "AND c.id = ?");
			
			ps.setInt(1, idCliente);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				cliente = mapRow(rs);				
			}
			
			rs.close();
			ps.close();			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return cliente;
	}
}
