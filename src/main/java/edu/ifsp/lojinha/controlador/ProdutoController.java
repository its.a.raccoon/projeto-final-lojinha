package edu.ifsp.lojinha.controlador;

import java.io.IOException;
import java.util.List;

import edu.ifsp.lojinha.controlador.produto.ExcluirCommand;
import edu.ifsp.lojinha.controlador.produto.SalvarProduto;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/produto.sec")
public class ProdutoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher rd = null;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		listAll(request);
		rd = request.getRequestDispatcher("listaprod.jsp");
		rd.forward(request, response);
	}
	
	private void listAll(HttpServletRequest request){
		ProdutoDAO dao = new ProdutoDAO();
		List<Produto> lista = null;
		try {
			System.out.println(dao.listAll().size());
			request.setAttribute("lista", dao.listAll());
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Command command = null;
		final String cmd = request.getParameter("cmd");

		if (cmd != null && "excluir".equals(cmd)) {

			String id = request.getParameter("id");
			command = new ExcluirCommand();
		} else {
			command = new SalvarProduto();
		}
		
		listAll(request);
		command.processar(request, response);
		rd = request.getRequestDispatcher("sucesso.jsp");
		rd.forward(request, response);
	}

}
