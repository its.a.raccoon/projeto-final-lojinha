package edu.ifsp.lojinha.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.ifsp.lojinha.modelo.Usuario;

public class UsuarioDAO {

	public Usuario check(String username, String password) throws PersistenceException {
		Usuario usuario = null;
		
		try (Connection conn = DatabaseConnector.getConnection()) {			
			PreparedStatement ps = conn.prepareStatement(
					"SELECT id, username FROM usuario WHERE username = ? AND password = ?;");
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			
			/* Se o usuário for encontrado */
			if (rs.next()) {
				/* recuperar dados cadastrais */
				usuario = new Usuario();
				usuario.setId(rs.getLong("id"));
				usuario.setUsername(rs.getString("username"));
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return usuario;
	}
	
	public boolean exists(String username) throws PersistenceException {
		boolean found = false;
		
		try (Connection conn = DatabaseConnector.getConnection()) {			
			PreparedStatement ps = conn.prepareStatement(
					"SELECT count(*) FROM usuario WHERE username = ?;");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next() && rs.getInt(1) > 0) {				
				found = true;
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return found;
	}

}
