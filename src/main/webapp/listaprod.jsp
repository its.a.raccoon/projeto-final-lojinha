<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Lista de Clientes</title>

<style type="text/css">
tr:nth-child(even) {
	background: yellow;
}
</style>
<script> 
</script>
</head>
<body>
	<h1>Lista de Produtos</h1>


	<table border="1">
		<tr>
			<th>Descricao</th>
			<th>Preço</th>
		</tr>

		<c:forEach items="${requestScope.lista}" var="p">
			<tr id="${p.id}">
				<td>${p.descricao}</td>
				<td>${p.preco}</td>
				<td><input id="prodId" name="prodId" value="${p.id}"
					type="hidden" /> 
					<a href="#" onclick="remover(${p.id})">remover</a>
				</td>
			</tr>
		</c:forEach>

	</table>
	
	<div class="scripts">
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<script>
		

		function remover(id) {
			console.log(id)
			$.ajax({
		        url: "produto.sec?cmd=excluir&id=" + id,
		        type: "POST",
		        success: function () {
		        	let tr = document.getElementById(id);
					tr.parentNode.removeChild(tr);
		         }, error: function(jqXHR, textStatus, errorThrown) {
		             alert('Deu ruim parceria')
		         }
		    });
		}
	</script>
		
	</div>
</body>
</html>