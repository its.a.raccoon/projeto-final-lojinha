package edu.ifsp.lojinha.controlador.produto;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import edu.ifsp.lojinha.web.FlashUtil;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class ExcluirCommand implements Command {
	
	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");
		
		try {
			ProdutoDAO produtoDao = new ProdutoDAO();		
			if (!produtoDao.exists(id)) {
				produtoDao.remove(Integer.parseInt(id));
			}
				
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}		
	}
}
