package edu.ifsp.lojinha.controlador;

import java.io.IOException;
import java.util.List;

import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.ClienteDAO;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/listar.sec")
public class ListarController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ProdutoDAO produtoDao = new ProdutoDAO();
			List<Produto> produtos = produtoDao.listarTodos();
			request.setAttribute("produtos", produtos);
			
			ClienteDAO dao = new ClienteDAO();
			List<Cliente> lista = dao.listAll();		
			request.setAttribute("lista", lista);		
			
			RequestDispatcher rd = request.getRequestDispatcher("lista.jsp");
			rd.forward(request, response);
		
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}
	}

}
